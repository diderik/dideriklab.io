---
title: The Invisible Hole in Doughnut Economics
categories:
- Blog
tags: []
comments: []
---
<p><span>Doughnut Economics is one of the best books I have read in the past few years. I believe it should be standard reading for any economics student and fills in a big gap in "normal" economics theory. Kate Raworth excellently points out how and why the traditional models and theories do not work (anymore) and even better, replaces them with new concepts and pictures.</span></p>
<p>&nbsp;</p>
<p><!--more--></p>
<h3 class="western"><span>The Doughnut in a Nutshell</span></h3>
<p><span>Doughnut Economics takes the planet and the human condition as the goal and explains how the economy should be serving these, instead of the other way around as it is nowadays portrayed by both economists and politicians. </span></p>
<p><span>I believe the strong point is that Raworth recognizes that only explaining how and why the old models and "laws" are wrong is not enough, she also replaces them with new models and most importantly pictures that are fit for the future and encourages us to take a pen and start drawing as well. </span></p>
<p><span>As the quote goes: "All models are wrong but some are useful" (George Box). I believe this book gives us those much needed new models in a holistic view where both the planet, humans, economy and government are included. Raworth does not come up with all of the materials herself, but cleverly compiled contemporary critiques, thoughts and models into a comprehensive and easy to read book. </span></p>
<p><span>In that sense Doughnut Economics does not provide a clear-cut solution, which the author strongly emphasizes. There is no 1 answer, 1 solution, in this great and complex world. Thinking or wanting to believe that there is such a thing, is actually the pitfall of the old theories.</span></p>
<p><span>Of all the great new economic models in the book, the one economic model that encompasses all and defines economy as serving all humans and limits it to planetary boundaries is the Doughnut, shown in the following image:</span></p>
<p align="center"><img class="alignnone wp-image-876" src="/media/images/blog/doughnut-model.jpg" alt="" width="413" height="412" /></p>
<p align="center"><span>The Doughnut of social and planetary boundaries (2017)<br />
</span><span>Source: kateraworth.com</span></p>
<p align="left"><span>As you can see the planetary boundaries (ecological ceiling) is defined as not &ldquo;overshooting&rdquo; in: climate change, ocean acidification, chemical pollution, nitrogen &amp; phosphorus loading, freshwater withdrawals, land conversion, biodiversity loss, air pollution and ozone layer depletion.</span></p>
<p align="left"><span>And the social foundation for humanity is defined as: housing, networks, energy, water, food, health, education, income &amp; work, peace &amp; justice, political voice, social equity and gender equality.</span></p>
<p align="left"><span>The &ldquo;safe and just space for humanity&rdquo; is then defined as the space between providing the social foundation for everyone while not overshooting the ecological ceiling.</span></p>
<p align="left"><span>I find this a very attractive model, way more inspirational and &lsquo;manageable&rsquo; than the often-used &lsquo;model&rsquo; of Sustainable Development Goals, shown in the following image:</span></p>
<p align="center"><img class="alignnone wp-image-878" src="/media/images/blog/sdg.jpg" alt="" width="535" height="265" /></p>
<p align="center"><span>The Sustainable Development Goals by the United Nations<br />
</span><span>Source: sustainabledevelopment.un.org</span></p>
<p align="left"><span>I also like how the Doughnut does not include a reference to &ldquo;Poverty&rdquo; at all, since <a href="{% post_url 2017-04-20-stop-trying-to-end-poverty %}">I believe that money is only a proxy</a> (nobody eats money or lives in it) and limits us to talking inside the current economic system, of which the flawed monetary system is a big part. </span></p>
<p><span>Besides debunking existing (neo-liberal) economic theories and models and presenting fitter ones, Raworth gives many examples of pioneering initiatives that pop up around the world. These examples serve as inspiration and anecdotal evidence of these fitter economic models. </span></p>
<p align="left"><span>As a big proponent of open collaboration and free knowledge sharing, I was very happy to read how Raworth strongly puts forward the role of Open Source Design as an essential building block for the future. She gives familiar examples like the Global Village Construction Set, the Open Building Institute and Open Source Circular Economy.</span></p>
<h3 class="western"><span>The Invisible Hole</span></h3>
<p><span>However, I also have some critical remarks on the book. The rest of this article will elaborate on those. But before that I want to emphasize my admiration for the incredible work of Raworth and it is only because she has put in this enormous amount of time and energy that I am able to write this at all.</span></p>
<p><span>My two main points of critique regard two of the core assumptions of the Doughnut Economics model. Namely, <b>in striving for a safe and just space for humanity</b>:</span></p>
<p><span><b>1. Can we actually safely provide a social foundation within planetary resources and without creating overshoot?</b></span></p>
<p><span><b>2. What exactly is a just social foundation in a practical sense and who decides that?</b></span></p>
<p><span>The book does not address these issues and worse, Raworth does not mention that the Doughnut is based on these assumptions. She does not, as academic writers normally do, provide a small portion of the writing on limitations and further research. So these implicit assumptions probably go by unnoticed to most readers.</span></p>
<p><span>I call this problem with the Doughnut model the &ldquo;Invisible Hole&rdquo;, as the two issues are interrelated and can be seen as one and the author has not made them explicit to the reader.</span></p>
<p><span>The following paragraphs will elaborate on my points of critique regarding these &lsquo;safe&rsquo; and &lsquo;just&rsquo; issues.</span></p>
<h3 class="western">Can we be safe?</h3>
<p><span>The &ldquo;Can we be safe within planetary boundaries?&rdquo;-issue already came to mind the first time I saw the Doughnut in an article on-line. The model states that we should at first provide in basic human needs for all people on the planet. </span></p>
<p><span>The author calls this the Social Foundation, providing in: sufficient food, clean water and decent sanitation, access to energy and clean cooking facilities, access to education and to healthcare, decent housing, a minimum income and decent work and access to networks of information and social support. Furthermore, it calls for achieving these with gender equality, social equity, political voice, and peace and justice (page 45).</span></p>
<p><span>This is the inner circle of the Doughnut. Then, there is some "wiggle room" in providing more within planetary boundaries, before we would go into "overshoot", taking too much from the planet. This is the outer circle of the Doughnut. </span></p>
<p><span>But how does the author know if we can actually provide in all these basic human needs within planetary boundaries and have this wiggle room left?</span></p>
<p><span>Maybe the stuff needed for the Social Foundation already causes overshoot and the inner circle should actually be outside the outer circle? </span></p>
<p><span>For example, do we have sufficient materials to make things that will provide a basic provision of energy to all on the planet, forever?</span></p>
<p><span>This question was so obvious to me and I expected a solid answer while reading the book. But that answer did not come. </span></p>
<p><span>Well, actually I did find one remark only regarding food supply, which is on page 56. Given that 30% to 50% of the world's food is wasted, she states that hunger could be ended with just 10% of the food that gets never eaten. </span></p>
<p><span>This seems acceptable at face value, but the current food system, especially meat production, is a major 'overshoot' factor. So the calculation should instead be based on a regenerative distributive kind of food system that Raworth talks about, not the current food system. Furthermore, the calculation is based on caloric intake and leaves out quality (and transportation) of food.</span></p>
<h3 class="western"><span>Who decides what is just?</span></h3>
<p><span>The &ldquo;What is just?&rdquo;-issue relates to my Safe-issue: the book assumes that "safe" and "just" go hand in hand and are possible well within planetary boundaries. But what exactly is a just amount of (for example) energy for each of us? And who decides that? Or is it by definition that "just" is below the ecological ceiling? </span></p>
<p><span>Since Raworth does not address what "just" means in a practical way, or how we should find out, we are left to guess. Actually, I only realized that I had an implicit personal WEIRD(*) assumption of what just means, until I almost finished the book.</span></p>
<p><span>(*) WEIRD: Western, Educated, Industrialized, Rich and Democratic (page 95).</span></p>
<p><span>Some primary very basic questions from my personal perspective are: in order to provide &ldquo;just&rdquo; access to basic human needs while being &ldquo;safe&rdquo;, should we give up watching TV? How about dishwashers? Showering every day? A house made with concrete? Flying? </span></p>
<h3 class="western"><span>Indicators of Shortfall</span></h3>
<p><span>Although Raworth does not define or at least elaborate on what she means with a just space for humanity or how or by whom that should be determined, the book does contain a table of data with indicators of shortfall in the appendix at the end (in my opinion: too little, too late).</span></p>
<p><span>These data are absolutely interesting to keep an eye on and these shortfalls should be overcome to enter the just space, but I would rather say they are necessary conditions than sufficient conditions. I believe that it is therefore useful to use other indicators as well to get a more complete 'dashboard' of where we are in achieving the social foundation. </span></p>
<p><span>As food for thought the table below contains a rewording of each indicator into a "just statement" contrasted with a brief summary of my personal WEIRD view. </span></p>
<p><span>This exercise is meant as a basis for a further dialogue on "what is just" and which other indicators would fit there. It is not meant to say that the author is wrong and I am right or anything like that.</span></p>
<table border="1" width="100%" cellspacing="0" cellpadding="4">
<colgroup>
<col width="57*" />
<col width="98*" />
<col width="101*" /> </colgroup>
<tbody>
<tr valign="top">
<td width="22%"><span><b>Basic Need</b></span></td>
<td width="38%"><span><b>Indicator &ldquo;just statement&rdquo;</b></span></td>
<td width="39%"><span><b>My WEIRD interpretation</b></span></td>
</tr>
<tr valign="top">
<td width="22%"><span>Food</span></td>
<td width="38%"><span>Everybody is well nourished (now 11% are not)</span></td>
<td width="39%"><span>Everybody has access to high-quality healthy food (this will then probably become a plant-based whole foods diet that is grown locally; with meat, fish, dairy and processed food available at much higher prices which not everyone can afford).</span></td>
</tr>
<tr valign="top">
<td width="22%"><span>Water</span></td>
<td width="38%"><span>Everybody has access to improved drinking water (now 9% do not)</span></td>
<td width="39%"><span>Everybody has direct access to clean water sufficient for drinking, bathing and washing</span></td>
</tr>
<tr valign="top">
<td width="22%"><span>Sanitation</span></td>
<td width="38%"><span>Everybody has access to improved sanitation (now 32% do not)</span></td>
<td width="39%"><span>Everybody has direct access to clean toilets where waste is regeneratively returned to its ecosystems </span></td>
</tr>
<tr valign="top">
<td width="22%"><span>Energy</span></td>
<td width="38%"><span>Everybody has access to electricity (now 17% do not)</span></td>
<td width="39%"><span>Everybody has sufficient electricity for their lighting, washing machine and devices</span></td>
</tr>
<tr valign="top">
<td width="22%"><span>Cooking facilities</span></td>
<td width="38%"><span>Everybody has access to clean cooking facilities (now 38% do not)</span></td>
<td width="39%"><span>Everybody has access to clean cooking facilities (so the same)</span></td>
</tr>
<tr valign="top">
<td width="22%"><span>Education</span></td>
<td width="38%">&ndash; <span>Every adult is able to read and write (now 15% cannot);<br />
&ndash; Every child goes to school at least until 15 years old (now 17% does not)</span></td>
<td width="39%"><span>Everybody has access to education, free information and guidance to become a productive, informed, critical-thinking citizen, with the opportunity of self-actualization.</span></td>
</tr>
<tr valign="top">
<td width="22%"><span>Healthcare</span></td>
<td width="38%">&ndash; <span>Less than 25 in 1,000 babies (live births) die before age 5 (now 46% living in countries where this number is higher);<br />
&ndash; Everybody has a life expectancy of over 70 years (now 39% living in countries where this is less)</span></td>
<td width="39%"><span>Everybody has access to modern healthcare</span></td>
</tr>
<tr valign="top">
<td width="22%"><span>Housing</span></td>
<td width="38%"><span>Nobody lives in slum housing (now 24% do)</span></td>
<td width="39%"><span>Everybody can live in a safe, comfortable house with direct access to water, sanitation, energy and cooking facilities.</span></td>
</tr>
<tr valign="top">
<td width="22%"><span>Income and work</span></td>
<td width="38%">&ndash; <span>Everybody lives above the international poverty limit of $3.10 per day (now 29% live below);<br />
&ndash; All young people (15-24) who seek work can find it (now 13% cannot)</span></td>
<td width="39%"><span>Everybody can readily provide in their basic human needs regardless of income and employment</span></td>
</tr>
<tr valign="top">
<td width="22%"><span>Networks of information</span></td>
<td width="38%"><span>Everybody has Internet access (now 57% do not)</span></td>
<td width="39%"><span>Everybody has access to reliable, uncensored and broadband Internet</span></td>
</tr>
<tr valign="top">
<td width="22%"><span>Social support</span></td>
<td width="38%"><span>Everybody has someone to count on for help in times of trouble (now 24% are not)</span></td>
<td width="39%"><span>Everybody has the opportunity to live in a supportive community.</span></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p><span>This concludes my two main points of critique: the Invisible Hole. The following paragraphs contain some other remarks I have about the book and a bit of reflection.</span></p>
<h3 class="western"><span>What about local government and communities?</span></h3>
<p><span>The book focuses on three units of analysis for a new economic system: households, companies and national governments. I found a lack of emphasis on local government and local communities. </span></p>
<p><span>I believe that shifting the unit of analysis from the &ldquo;rational economic man&rdquo; to that of the household with its more complex dynamics hidden to traditional economics is very valuable. Also, I agree that there is a strong but different kind of role to play by government in a safe and just society (I am not an anarchist). </span></p>
<p><span>However, I would say that the distributive and regenerative society that Raworth proposes would depend a lot on resilient and self-reliant local communities and local government acting as a &lsquo;partner&rsquo; as is nowadays talked about a lot. </span></p>
<p><span>These communities and government would need to figure out together how to let amongst others local agriculture, manufacturing and managing the Commons flourish. The book instead pretty much leaves out local communities in which the households are embedded and government focus is almost completely on nation states.</span></p>
<h3 class="western"><span>What about Mobility?</span></h3>
<p><span>Regarding the basic human needs, the one that I found to be missing is Mobility. Would it not be just if people would be able to explore a bit more of the world than just their own town? Or should we regard this a luxury only for the privileged?</span></p>
<h3 class="western"><span>Do I have a point?</span></h3>
<p><span>Asking myself &ldquo;Do I have a point?&rdquo; is a bit odd. Because I would not have written this article if I thought I had not. Even though this is only a relatively short writing up of my own thoughts in my little spare time, I feel that the described Invisible Hole in the model is a valid critique.</span></p>
<p><span>But perhaps I am misunderstanding the model, or put too much emphasis on these aspects? What do you think?</span></p>
<p><span>I spent some time finding and reading other reviews of Doughnut Economics and see what they are about. Many reviews are superficial and contain nothing more than a brief introduction and praise. </span></p>
<p><span>But I did find some more interesting ones:</span></p>
<ul>
<li><a class="western" href="https://www.resilience.org/stories/2017-06-30/doughnut-economics-a-step-forward-but-not-far-enough/">&ldquo;</a><span><span lang="en-US"><a class="western" href="https://www.resilience.org/stories/2017-06-30/doughnut-economics-a-step-forward-but-not-far-enough/">Doughnut Economics: a Step Forward, but Not Far Enough&rdquo;</a> </span><span lang="en-US">by Ugo Bardi: this review is the one I found closest to the points I am making. It addresses the issue of resource depletion and how it is missing from the book (and even contemporary dialogue in general). Furthermore, I believe the author is spot on questioning the logic in the circular shape of the Doughnut. The Doughnut might just as well been a candy bar or a Reece&rsquo;s Piece. The circular design of the model does not really have a function, other than perhaps that it is visually attractive and makes people think it relates to circularity (which much of the thinking of Raworth does, but the model does not show that).</span></span></li>
<li><a class="western" href="https://blogs.worldbank.org/publicsphere/4-2017-review-doughnut-economics-new-book-you-will-need-know-about">&ldquo;</a><span><span lang="en-US"><a class="western" href="https://blogs.worldbank.org/publicsphere/4-2017-review-doughnut-economics-new-book-you-will-need-know-about">Review of Doughnut Economics &ndash; a new book you will need to know about&rdquo;</a> </span><span lang="en-US">by Duncan Green: a nice summary of some key points of the book, along with the emotional opinions of the author and links to short informative videos of the book.</span></span></li>
<li><a class="western" href="https://www.thenationalbookreview.com/features/2017/10/5/review-doughnut-economics-a-smart-fresh-take-on-finacial-matters">&ldquo;</a><span><span lang="en-US"><a class="western" href="https://www.thenationalbookreview.com/features/2017/10/5/review-doughnut-economics-a-smart-fresh-take-on-finacial-matters">'DOUGHNUT ECONOMICS': A HUMANE, 21ST CENTURY TAKE ON THE DISMAL SCIENCE&rdquo;</a> </span><span lang="en-US">by James O&rsquo;Shea: this author starts with a nice personal story as an economics journalist and how the book created that &lsquo;aha moment&rsquo; for him, then summarizes some key statements and findings in the book.</span></span></li>
<li><a class="western" href="https://fee.org/articles/theres-a-hole-in-the-middle-of-doughnut-economics/">&ldquo;</a><span><span lang="en-US"><a class="western" href="https://fee.org/articles/theres-a-hole-in-the-middle-of-doughnut-economics/">There's a Hole in the Middle of Doughnut Economics&rdquo;</a> </span><span lang="en-US">by Steven Horwitz: this is an interesting one, as the review pretty much dismisses the entire book claiming that GDP/economic growth is the only proven way of reducing poverty and having more people with sufficient food, clothing, shelter etc. My cognitive dissonance with this claim immediately makes me think of all the pages Raworth spends on explanation including (academic) references and the lack of these in the article of Horwitz. </span></span></li>
</ul>
<p><span>- Diderik</span></p>
<p><span>*** Special gratitude to Jaime Arredondo for doing an excellent review of a draft version of this article. Thanks to his contribution I was able to greatly improve the article&rsquo;s structure, focus on the essentials and better articulate many points. Now that you have finished reading this article, go and read his awesome work on <a href="http://boldandopen.com/">Bold &amp; Open</a>!</span></p>
<p>Photo by&nbsp;<a href="https://unsplash.com/photos/HauxSOFvh6k?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Jez Timms</a>&nbsp;on&nbsp;<a href="https://unsplash.com/search/photos/black-hole?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a></p>
