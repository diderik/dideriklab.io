---
title: Should you donate to Open Source Software?
categories:
- Blog
tags: []
comments: []
---
<p>In short: yes, you should! If you are a regular user and can afford it. For the longer version: read on. In this article I will explain donating is not just &ldquo;the right thing to do&rdquo;, but also a practical way of supporting Open Source Software (OSS). I will show you a fair and pragmatic method that I use myself. You will see that donating does not need to cost you much (in my case less than &euro; 25 per month; 25% of the proprietary alternatives), is easy and gets this topic &ldquo;off your mind&rdquo; for the rest of the time.</p>
<p><!--more--></p>
<p>Using LibreOffice as an example you will also see that even if only the 5 governments mentioned on the LibreOffice website would follow my method, then this would bring in almost 10 times more than would be needed to pay all the people working on the project a decent salary, even when living in a relatively &lsquo;expensive&rsquo; country like The Netherlands!</p>
<h3 class="western">&ldquo;When a project is valuable it will find funds&rdquo;</h3>
<p>From a user&rsquo;s perspective it seems fair to donate to Open Source Software: you get utility value from using these programs and you would otherwise need to buy proprietary software. So some reciprocity simply feels like the right thing to do. For me not only the utility value matters, but the whole idea of <a class="western" href="https://www.gnu.org/philosophy/free-sw.en.html">software freedom</a> resonates.</p>
<p>On occasion I raised the question: &ldquo;How much should one donate?&rdquo; in the Free Software Foundation Europe (FSFE) community. I was surprised to find out that this is a question of little concern there. The reply that I got was something like: &ldquo;When a project is valuable it will find the necessary funds anyway.&rdquo; From my user&rsquo;s perspective I did not understand this reaction, until I started doing some research for writing this piece.</p>
<p>It boils down to this: every contributor to OSS has her <a class="western" href="https://webmink.com/essays/monetisation/">own reasons for contributing</a>, without expecting to be paid by the project. Maybe she wants to learn, is a heavy user of the program herself, is being paid for it by her employer (when this company sells products or services based on the OSS) or simply enjoys the process of creation.</p>
<p>The accepted fact is that donations will never be able to provide a livelihood for the contributors, as they are insufficient and vary over time. I even read that bringing in too much money could <a class="western" href="https://opensource.com/life/11/10/should-you-donate-open-source-projects">jeopardize the stability of a project</a>, as the questions of: who gets to decide what to spend the money on and where is the money going, could result in conflict.</p>
<h3 class="western">Contribute, otherwise donate</h3>
<p>So, as a user should you donate to OSS at all then? My answer: yes, if you can afford it. However, the real scarcity for OSS development is not money, it is time. The time to develop, test, write documentation, translate, do community outreach and more. If you have a skill that is valuable to an OSS project and you can free up some time, then <a class="western" href="https://opensource.com/business/13/7/donations-open-source-projects">contribute with</a><a class="western" href="https://opensource.com/business/13/7/donations-open-source-projects"> your time</a> performing this skill.</p>
<p>In case you are not able or willing to contribute, then donate money if the project asks for it and you can afford it. My take is that if they ask for donations, they must have a reason to do so as part of maintaining the project. It is the project&rsquo;s responsibility to decide on spending it wisely.</p>
<p>Furthermore, while you may have the option to go for a proprietary alternative if the need comes, many less fortunate people do not have this option. By supporting an OSS project you aid in keeping it alive, indirectly making it possible for those people to also use great quality software in freedom.</p>
<p>For me personally, I decided to primarily donate money. On occasion I have donated time, in the form of <a class="western" href="/talks/">speaking about Open Source</a> at conferences. For the rest, I do not have the necessary skills.</p>
<h3 class="western">To which projects to donate?</h3>
<p>The question then arises, to which projects to donate? I have taken a rather crude yet pragmatic approach here, donating only to those projects for which I consciously take a decision to install it. Since every OSS project typically depends on so many other pieces of OSS (called &lsquo;upstream packages&rsquo;), donating to all projects gets very complicated very quickly, if not practically impossible. Furthermore, I feel it as a responsibility of the project to decide on donating part of their incoming funds to packages that they depend on.</p>
<p>For example, if GIMP (photo editing software) would come pre-installed with the GNU/Linux distribution I am using, then I would not donate to GIMP. If on the other hand GIMP would not come pre-installed and I had to install it manually with the (graphical interface of the) package manager, then I would donate to GIMP. Is this arbitrary? It absolutely is, but at the moment I do not know of any pragmatic alternative approach. If you do, please share!</p>
<p>I also take my time in evaluating software, meaning that it could take months until it becomes apparent that I actually use a piece of software and come back to it regularly. I feel this as a major &ldquo;freedom&rdquo;: paying based on proven utility.</p>
<h3 class="western">How much to donate?</h3>
<p>After deciding to which projects to donate, the next question becomes: how much to donate? One of the many positive stated attributes of Open Source is that while enabling freedom and being more valuable, it is also cheaper to create than proprietary counterparts. There is no head office, no bureaucratic layers of managers, no corporate centralized value extraction, no expensive marketing campaigns and sales people, no legal enforcement costs. So for software that has many users globally, even if the software developers would be paid the costs per user would be very low.</p>
<p>I once read that Open Source Ecology is aiming to create the technology and know-how to be able to produce machines at 25% of the costs of proprietary commercial counterparts. Although completely arbitrary, I feel this 25% to be a reasonable assumption. It is well less than half of the proprietary solution, but certainly not nothing.</p>
<p>So, for example. I use LibreOffice for document writing, spreadsheets and presentations. A proprietary alternative would be Microsoft Office. The one-time price for a new license of Microsoft Office 2016 is &euro; 279 in The Netherlands. I have no idea how long this license would last, but let&rsquo;s assume a period of 5 years. I choose to donate monthly to OSS, so this means that for LibreOffice I donate: &euro; 279 / 5 / 12 = &euro; 4,65 per month.</p>
<p>The table below shows this calculation for all OSS I use regularly:</p>
<table width="624" cellspacing="0" cellpadding="4">
<colgroup>
<col width="152" />
<col width="173" />
<col width="132" />
<col width="133" /> </colgroup>
<tbody>
<tr valign="top">
<td width="152"><b>Open Source Software</b></td>
<td width="173"><b>Proprietary alternative (*)</b></td>
<td width="132"><b>Proprietary Price</b></td>
<td width="133"><b>Donate 25% monthly</b></td>
</tr>
<tr valign="top">
<td width="152"><a class="western" href="https://elementary.io/">Elementary OS</a></td>
<td width="173">Windows 10 Pro</td>
<td width="132">&euro; 259 (5 years)</td>
<td width="133">&euro; 4,35</td>
</tr>
<tr valign="top">
<td width="152"><a class="western" href="https://www.libreoffice.org/">LibreOffice</a></td>
<td width="173">Microsoft Office 2016</td>
<td width="132">&euro; 279 (5 years)</td>
<td width="133">&euro; 4,65</td>
</tr>
<tr valign="top">
<td width="152"><a class="western" href="https://www.gimp.org/">GIMP</a></td>
<td width="173">Adobe Photoshop Photography</td>
<td width="132">&euro; 12 per month</td>
<td width="133">&euro; 3</td>
</tr>
<tr valign="top">
<td width="152"><a class="western" href="http://nixnote.org/">NixNote</a></td>
<td width="173">Evernote Premium</td>
<td width="132">&euro; 60 per year</td>
<td width="133">&euro; 5 (**)</td>
</tr>
<tr valign="top">
<td width="152"><a class="western" href="http://www.workrave.org/">Workrave</a></td>
<td width="173">Workpace</td>
<td width="132">&euro; 82 (5 years)</td>
<td width="133">&euro; 1,40</td>
</tr>
<tr valign="top">
<td width="152"><a class="western" href="https://www.keepassx.org/">KeePassX</a></td>
<td width="173">1Password</td>
<td width="132">$ 3 per month</td>
<td width="133">&euro; 2,60</td>
</tr>
<tr valign="top">
<td width="152"><a class="western" href="http://slic3r.org/">Slic3r</a></td>
<td width="173">Simplify3D</td>
<td width="132">$149 (5 years)</td>
<td width="133">&euro; 2,15</td>
</tr>
<tr valign="top">
<td width="152"></td>
<td width="173"></td>
<td width="132"></td>
<td width="133"></td>
</tr>
<tr valign="top">
<td width="152"><b>Total monthly donation</b></td>
<td width="173"></td>
<td width="132"></td>
<td width="133">&euro; <b>23,15</b></td>
</tr>
<tr valign="top">
<td width="152"><b>Equals total per year</b></td>
<td width="173"></td>
<td width="132"></td>
<td width="133">&euro; <b>277,80</b></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>(*) Calculating the amount to donate based on the proprietary alternative is my pragmatic choice. Another approach could be to donate based on &lsquo;value received&rsquo;. Although I feel that this could be more fair, I do not know how to make this workable at the moment. I think it would quickly become too complex to do, but if you have thoughts on this, please share!</p>
<p>(**) I am using NixNote without the Evernote integration. Evernote Premium has a lot more functionality than NixNote alone. Especially being able to access and edit notes on multiple devices would be handy for me. The Freemium version of Evernote compares more to NixNote. However, Evernote Freemium is not free, it is a marketing channel for which the Premium users are paying. To keep things simple, I decided to donate 25% of Evernote Premium.</p>
<h3 class="western">What would happen if every user did this?</h3>
<p>Continuing from the LibreOffice example, I found that <a class="western" href="https://news.softpedia.com/news/libreoffice-now-has-more-than-1-million-of-active-users-496910.shtml">LibreOffice had over 1 million active users</a> in 2015. The total <a class="western" href="https://blog.documentfoundation.org/blog/2016/01/01/libreoffice-the-numbers/">amount of donations coming in</a> on the last quarter of 2015 was $ 21,000, which is about &euro; 6,000 per month, or less than 1 euro cent per active user. If instead every active user would donate &lsquo;my&rsquo; &euro; 4,65 then over &euro; 4.5 million would be coming in every month!</p>
<p>I am not saying that every user should do this, many people who are reaping the benefits of OSS cannot afford to donate and everyone else has the freedom to make up their own mind. That is the beauty of Open Source Software.</p>
<h3 class="western">What would happen if those who can afford it did this?</h3>
<p>Okay, so it is not fair to do this calculation based on every active user of LibreOffice. Instead, let&rsquo;s do a calculation based on the active users who can afford it, minus the people who already reciprocate with a contribution in time and skills.</p>
<p>To start with the latter group: the number of LibreOffice developers stood at <a class="western" href="https://blog.documentfoundation.org/blog/2016/01/01/libreoffice-the-numbers/">about 1,000</a> in November 2015. If we assume that half of the people involved are developers and half are people doing &lsquo;everything else&rsquo; (testing, documentation writing, translating, etc.), then this translates to 0.2% of users. In other words: no significant impact on the calculations.</p>
<p>Then the former group: how can we know who can afford to donate? I believe we can safely assume that at least large institutions like governments can donate money they otherwise would have spent on Microsoft licenses. I found a <a class="western" href="https://www.libreoffice.org/discover/who-uses-libreoffice/">list of governments using LibreOffice</a>: in France, Spain, Italy, Taiwan and Brazil. They are using LibreOffice on a total of no less than 754,000 PCs! If these organizations would be donating that &euro; 4,65, the project would still bring in &euro; 3.5 million per month!</p>
<p>Am I missing something here? How is it possible that the LibreOffice project, one of the best-known OSS projects making software for everyday use by pretty much anyone is still only bringing in &euro; 6,000 per month? Did they forgot to mention &ldquo;amounts are *1000&rdquo; in the graph&rsquo;s legend?</p>
<p>Or do large institutions directly pay the salary of LibreOffice contributors, instead of donating? Let&rsquo;s have a look at the numbers of that (hypothetical) situation. In November 2015 LibreOffice had 80 &lsquo;committers&rsquo;, so developers committing code. I imagine that these developers are not working full working weeks on LibreOffice, let&rsquo;s assume 50% part-time, being 20 hours per week.</p>
<p>We again assume that development is half of the work being done. This translates to 80 people working full-time on the LibreOffice project at any given time. If they would be employed in The Netherlands, a reasonable average salary could be something like &euro; 4,500 (before tax), meaning a total cost of &euro; 360,000 per month.</p>
<p>Conclusion: even if only the 5 governments mentioned on the LibreOffice website would donate only 25% of the costs of the popular proprietary counterpart (Microsoft Office), then this would bring in almost 10 times more than would be needed to pay all the people working on the project a decent salary, even when living in a relatively &lsquo;expensive&rsquo; country like The Netherlands.</p>
<p>How amazing is this? I almost cannot believe these numbers.</p>
<h3 class="western">Setting up automated monthly donations</h3>
<p>Anyway, back to my pragmatic approach for monthly donations to OSS you regularly use. Now that we know which amount to give to which projects, the task is actually doing so.&nbsp;The following table shows the donation options for the OSS I regularly use:</p>
<table width="100%" cellspacing="0" cellpadding="4">
<colgroup>
<col width="108*" />
<col width="148*" /> </colgroup>
<tbody>
<tr valign="top">
<td width="42%"><b>Open Source Software</b></td>
<td width="58%"><b>Donation options</b></td>
</tr>
<tr valign="top">
<td width="42%">Elementary OS</td>
<td width="58%"><a class="western" href="https://elementary.io/get-involved">Patreon, PayPal, Goodies, BountySource</a></td>
</tr>
<tr valign="top">
<td width="42%">LibreOffice</td>
<td width="58%"><a class="western" href="https://www.libreoffice.org/donate/">PayPal, Credit Card, Bitcoin, Flattr, Bank transfer</a></td>
</tr>
<tr valign="top">
<td width="42%">GIMP</td>
<td width="58%"><a class="western" href="https://www.gimp.org/donating/">Patreon (developers directly), PayPal, Flattr, Bitcoin, Cheque</a></td>
</tr>
<tr valign="top">
<td width="42%">NixNote</td>
<td width="58%">Not accepting donations (*)</td>
</tr>
<tr valign="top">
<td width="42%">Workrave</td>
<td width="58%">Not accepting donations (*)</td>
</tr>
<tr valign="top">
<td width="42%">KeePassX</td>
<td width="58%">Not accepting donations (*)</td>
</tr>
<tr valign="top">
<td width="42%">Slic3r</td>
<td width="58%"><a class="western" href="http://slic3r.org/">PayPal</a></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>(*) Interestingly not all projects accept donations. The people at Workrave suggest donation to the Electronic Frontier Foundation instead, but the EFF only accepts donations of $ 5 per month or more. For now, these projects will not receive a donation.</p>
<p>Donating is not something you want to do manually every month, so an automatic set-up is ideal. In the above cases I think only Patreon and PayPal allow this. Even though PayPal is cheaper than Patreon and <a class="western" href="https://teamhuman.fm/episodes/ep-94-team-human-virtual-futures-live-in-london-part-1-pat-cadigan/">according to Douglas Rushkoff</a> Patreon is becoming problematic as an extractive multi-billion-dollar valued entity with <a class="western" href="https://www.crunchbase.com/organization/patreon#section-locked-charts">over</a> <a class="western" href="https://www.crunchbase.com/organization/patreon#section-locked-charts">$ 1</a><a class="western" href="https://www.crunchbase.com/organization/patreon#section-locked-charts">00 million invested</a> in it, I prefer this option over PayPal. The reason is that donating via Patreon gives &lsquo;marketing value&rsquo; to the project, as the donations and donors are visible, boosting general recognition.</p>
<p>Even though I have not done any research (yet), I will go with Rushkoff that <a class="western" href="https://d.rip/">Drip</a> is the preferred option over Patreon, so switch to that when it becomes available for these projects.</p>
<p>I will evaluate the list once or perhaps twice a year, as to keep the administrative burden manageable. For me personally once I get used to a piece of software and get the benefits from it, I rarely go look for something else.</p>
<h3 class="western">How can OSS projects bring in more donations?</h3>
<p>Presuming that a project is interested in bringing in more donations, my thoughts from my personal donating experience are:</p>
<ul>
<li>Being available on Drip or otherwise Patreon, because these are also communication channels besides mere donation tools.</li>
<li>Nudging downloaders towards leaving their e-mail address, then follow up after a few months. For me personally, I want to try out the software first. Once I find out I really like it and use it regularly, I am absolutely willing to pay for it.</li>
<li>Provide a suggested donation, explain why that amount it suggested and what it will be used for. And of course nudge (potential) donors towards contributing, making it transparent and super easy to figure out how contributions can be done and provide value.</li>
</ul>
<p>So, what do you think? Is it fair to pay for Open Source Software that you are using? If so, which approach would you advocate? And if not, if projects are asking for donations, why not donate?</p>
<p>&ndash; Diderik</p>
<p>Photo by&nbsp;<a href="https://unsplash.com/photos/FBiKcUw_sQw?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Nathan Lemon</a>&nbsp;on&nbsp;<a href="https://unsplash.com/search/photos/abundance?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a></p>
