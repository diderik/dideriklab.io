---
title: Reinventing Startups
header:
  image: media/images/blog/reinventing-startups.jpg
  teaser: media/images/blog/reinventing-startups-teaser.jpg
categories:
- Blog
tags: []
comments: []
---

Once we have come to realize that the Silicon Valley model of startup-based innovation is not for the betterment of people and planet, but is a hyper-accelerated version of the existing growth-based capitalist Operating System increasing inequality, destroying nature and benefiting the 1%, the question arises: is there an alternative?

One group of people that have come to this realization is searching for an alternative model. They call their companies Zebras, opposing the Silicon Valley $ 1 Billion+ valued startups called Unicorns. Their movement is called [Zebras Unite](https://www.zebrasunite.com/) and is definitely worth checking out. I recently wrote a ‘Fairy Tale’ story to give Zebras Unite a warning as I believe they run the risk of leaving the root cause of the current faulty Operating System untouched. This warning is based on the [brilliant work of Douglas Rushkoff](http://www.rushkoff.com/books/throwing-rocks-at-the-google-bus/).

This article proposes an alternative entrepreneurial-based innovation model that I believe has the potential to be good for people and planet by design. This model goes beyond the lip service of the hyped terms ‘collaboration’, ‘openness’, ‘sharing’ and ‘transparency’ and puts these terms in practice in a structural and practical way. The model is based on the following premises, while at the same time leaving opportunity for profitable business practices:

- **Freely sharing all created knowledge and technology under open licenses**: if we are going to solve the tremendous ‘wicked’ problems of humanity, we need to realize that no single company can solve these on their own. We need to realize that getting to know and building upon each others’ innovations via coincidental meeting, liking and contract agreements is too slow and leaves too much to chance. Instead, we need to realize that there is no time to waste and all heads, hands and inventiveness is necessarily shared immediately and openly to achieve what I call Permissionless Innovation.
- **Being growth agnostic**: if we are creating organizations and companies, these should not depend on growth for their survival. Instead they should thrive no matter what the size, because they provide a sustained source of value to humanity.
- **Having interests aligned with people and planet**: for many corporations, especially VC-backed and publicly traded companies, doing good for owners/shareholders and doing good for people and the planet is a trade-off. It should not be that way. Providing value for people and planet should not conflict with providing value for owners/shareholders.
Starting from these premises, I propose the following structural changes for customer-focused organizations and companies:

## Intellectual Property
We should stop pretending that knowledge is scarce, whereas it is in fact abundant. Putting open collaboration in practice means that sharing is the default: publish technology/designs/knowledge under Open Licenses. This results in what I call ‘Permissionless Innovation’ and favors inclusiveness, as innovation is not only for the privileged anymore. Also, not one single company can pretend to have the knowledge to solve ‘wicked problems’, we need free flow of information for that. And IP protection also creates huge amounts of ‘competitive waste’.

## Sustainable Product Development
Based on abundant knowledge we can co-design products that are made to be used, studied, repaired, customized and innovated upon. We get rid of the linear ‘producer => product => consumer’ and get to a ‘prosumer’ or ‘peer to peer’ dynamic model. This goes beyond the practice of Design Thinking / Service Design and crowdsourcing and puts the power of design and production outside of the company as well as inside it (open boundaries).

## Company Ownership
Worker and/or customer owned cooperatives as the standard, instead of rent-seeking hands-off investor shareholder-owners. With cooperative ownership the interests of the company are automatically aligned with those of the owners. I do not have sufficient knowledge about how this could work with start-up founders who are also owners, but at some point the founders should transition their ownership.

## Funding
Companies that need outside funding should be aware of the ‘growth trap’ as described above. Investment should be limited in time (capped ROI) and loans paid back. To be sustainable and ‘circular’ the company in the long run should not depend on loans or investments. Ideally in the future even the use of fiat money should be abandoned, in favor of alternative currency that does not require continuous endless growth.

## Scaling
Getting a valuable solution in the hands of those who need it, should not depend on a centralized organization having control, the supply-chain model. Putting ‘distributive’ in practice means an open dynamic ecosystem model supporting local initiative and entrepreneurship, while the company has its unique place in the ecosystem. This is similar to the franchise model, but mainly permissionless except for brands/trademarks. This dynamic model is much more resilient and can scale faster, especially relevant for non-digital products.

– Diderik

Photo from [Stocksnap.io](https://stocksnap.io/).

This article first appeared on [reinvent.cc](https://reinvent.cc/startups/2018/10/10/reinventing-startups.html).