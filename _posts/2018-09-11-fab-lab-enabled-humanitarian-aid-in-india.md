---
title: Fab Lab-enabled Humanitarian Aid in India
header:
  image: media/images/blog/fab-lab-enabled-humanitarian-aid-in-india.jpg
  teaser: media/images/blog/fab-lab-enabled-humanitarian-aid-in-india-teaser.jpg
categories:
- Blog
tags: []
comments: []
---

Since June 2018 the state of Kerala in India has endured massive floodings, as you may have [read](https://www.bbc.com/news/world-asia-india-45243868) in the [news](https://www.theatlantic.com/photo/2018/08/devastating-monsoon-floods-in-kerala-india/568171/). This article contains a brief summary what the international [Fab Lab Community](http://www.fabfoundation.org/) has been doing until now (early September 2018) to help the people recovering.

Note 1: I am writing this from my point of view, from what I am remembering that has happened. In case you have any additions or corrections, [contact me](/contact/).

Note 2: Unfortunately the conversations are taking place in a closed Telegram group, making it difficult for other people to read back on what happened, study it and learn from it. An [attempt](https://discuss.fablabs.io/t/kerala-flood-relief/8458) has been made to move the conversation to the Fab Lab Forum, but that was not successful.

It started with my search for [non-chemical plywood in The Netherlands](https://discuss.fablabs.io/t/non-chemical-plywood-in-benelux-area/8403) for which I contacted many of the Fab Labs in the BeNeLux. In a response someone gave me an invite link to a Telegram group consisting of Fabbers from all over the world. I joined the group and started following the conversation. Soon people from Fab Labs in India reported about the floodings and asked the international Fab Lab Community there for ideas and help. A seperate Telegram group “Fab for Kerala” was created and several dozens of people joined.

It was interesting to follow the progress of the conversation. At first the ideas were all over the place, as if ‘we’ were the only people going to provide assistance to the affected people in Kerala. Luckily that soon changed towards recognizing the unique strengths and position of local Fab Labs, understanding that not only we are operating in a very complex physical situation on the flood-affected ground, but also in a complex situation of various organizations providing help. The need to get into contact with these organizations was understood and steps taken.

There was also a call for ‘going out there and knowing what is going on locally’ and ‘getting an overview of the data’ first, before anything could be done. Local Fabbers responded that it was very difficult to get to know first what was going on and we should just get started providing the basic needs. These two came together by people putting forward ideas of what practical things could be done and people putting forward understandings of what they heard that people needed.

The ideas that came forward were amongst others:

- Building DIY gravity lamps so people who do not have electricity can have light at night.
- Building temporary houses for people who lost their house altogether.
- Various water filtration systems so people can make their own drinking water.

The needs that came forward were amongst others:

- A way to detect snakes in the houses that were flooded: as the water subsided the snakes hid in moist dark places and people got bit.
- Innovative ways to quickly clean a house of the mud that was left after the water subsided
- A quick way to give people an alternative form of shelter, as they were moving from the centralized government issued shelters back to their own neighbourhoods.

The idea for temporary housing and need for alternative shelter came together and there was a brainstorm on how to proceed. With the combined brain resources in the group the conclusion was quickly drawn that building family-size domes was the best way to go.

Various Fab Labs in India then made practical arrangements on where to source, how to transport and where to build 5 domes. A volunteer from [Fab Lab Kerala](http://fablabkerala.in/) got the government interested and accomplished that upon showing the success of the domes, the government would finance building more domes.

Various volunteers from these local Fab Labs then set out to get funding for building 5 domes. This proved to be a challenging task, as it appeared to very difficult and costly to move money into India. I do not really understand the specifics of the difficulties, but understood that the government of India is actively preventing money from coming into the country. A pragmatic solution was found, which relies a lot on trust and ‘social contract’ between the members of the Fab Labs.

At this moment volunteers from [Vigyan Ashram Fab Lab](http://vigyanashram.com/) (in India, but outside of Kerala) have constructed ‘dome kits’ for the skeletons. These kits are being transported to Fab Lab Kerala, so contruction can begin. In the meantime practical challenges are being faced, such as which material to use as ‘walls’ for the domes.

One of the volunteers involved with building the domes has indicated that he will send out regular updates. So, hopefully I can soon link to those from here.

I have been following this group in awe of the amazing willingness and organizing capacity of the international Fab Lab Community.

Hopefully this initiative will continue and people in need will get help. The shift in direction that I have seen from “be all to everything” towards focus on the Fab Labs’ unique position and core competences, I feel has been vital. As is the intent of communication and coordination with other organizations active in the area.

Maybe this could be the dawn of a new category of humanitarian aid and humanitarian development, with the Fab Lab and Maker values at its core. Not to ‘disrupt and replace’, but to provide a different perspective, bring new value and fresh ideas and solutions.

– Diderik

P.S.:

Pieter van der Hijden has made an effort to gather [information resources relevant](https://discuss.fablabs.io/t/kerala-flood-relief/8458) to this initiative. For the complete overview, see the international Fab Lab forum. Here a list of the resources:

- To log events and data by time and geo-location: [ushahidi.com](http://www.ushahidi.com/)
- To get a first insight into humanitarian organization practices: The Sphere Handbook, [sphereproject.org](http://www.sphereproject.org/)
- For local maker-based solution in humanitarian relief, the Field Ready training package, [fieldready.org](https://www.fieldready.org/)
- To take part in distributed testing: [humanitarianmakers.org](https://www.humanitarianmakers.org/)
- For manuals on getting safe drinking water, preventing infections, building a toilet, etc: [hesperian.org](https://hesperian.org/)
- For NGO-approved online training for Humanitarian Workers and Volunteers, for free (gratis): [disasterready.org](https://www.disasterready.org/)
- For an online resource to help individuals and organisations define humanitarian problems and successfully develop innovative solutions: [higuide.elrha.org](https://higuide.elrha.org/)

Photo from article [“For Kerala’s flood disaster, we have ourselves to blame”](https://www.hindustantimes.com/india-news/what-is-behind-the-kerala-monsoon-fury/story-2NxvHfTDAmS10k9hHofiiO.html) under fair use.