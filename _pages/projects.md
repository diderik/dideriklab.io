---
title: "Projects"
permalink: /projects/
layout: collection
collection: projects
author_profile: true
entries_layout: grid
classes: wide
sort_by: date
sort_order: reverse
show_excerpt: false
---