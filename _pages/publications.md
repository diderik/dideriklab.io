---
title: "Publications"
permalink: /publications/
layout: collection
collection: publications
author_profile: true
entries_layout: grid
classes: wide
sort_by: date
sort_order: reverse
show_excerpt: true
---
