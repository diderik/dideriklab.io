---
layout: splash
title: "Diderik van Wingerden"
permalink: /
header:
  overlay_color: "#5e616c"
  overlay_image: /media/images/home-page-feature.jpg
  actions:
  caption:
excerpt: 'Pragmatic Idealist working on Digital and Open Source Innovation. Hire me as your Technical Project Lead or Open Design Expert. My Superpower is analysing and structuring. <br><br>I have a solid background in Software Development, Business Economics and Lean Innovation approaches and know how to integrate these.'
feature_row:
  - image_path: /media/images/projects-feature.jpg
    alt: "projects"
    title: "Projects"
    excerpt: "Find out about projects I have done for clients and from my own initiative."
    url: "/projects/"
    btn_class: "btn--primary"
    btn_label: "Read More"
  - image_path: /media/images/talks-feature.jpg
    alt: "talks"
    title: "Talks"
    excerpt: "Watch videos and view or download sides of my talks."
    url: "/talks/"
    btn_class: "btn--primary"
    btn_label: "Read More"
  - image_path: /media/images/blog-feature.jpg
    alt: "blog"
    title: "Blog"
    excerpt: "Read about my professional experiences, opinions and ideas."
    url: "/blog/"
    btn_class: "btn--primary"
    btn_label: "Read More"
github:
intro:
  - excerpt: 'Stay updated or book an hour of advice for free &nbsp; [<i class="fab fa-twitter"></i> @diderikvw](https://twitter.com/diderikvw){: .btn .btn--twitter} [<i class="fas fa-fw fa-envelope-square"></i> Book Me](mailto:mailto:diderik (do that add thing) think-innovation.com?subject=Book me an hour of free advice&body=Please provide some details... and remember to fix the e-mail address with an @ instead of (do that add thing)){: .btn .btn--primary}'
---

{% include feature_row id="intro" type="center" %}

{% include feature_row %}